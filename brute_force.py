
from collections import namedtuple
from math import sqrt, ceil

Circle = namedtuple('Circle', 'x y radius')


def dist(x0, y0, x1, y1):
    diff_x = x1 - x0
    diff_y = y1 - y0
    return sqrt(diff_x * diff_x + diff_y * diff_y)


def is_in_circle(circle, x, y):
    dist_to_centre = dist(circle.x, circle.y, x, y)
    return dist_to_centre <= circle.radius


def red_green_count_in_circle(circle, X, Y, colours):
    reds = 0
    greens = 0
    for i in range(len(X)):
        if is_in_circle(circle, X[i], Y[i]):
            if colours[i] == 'R':
                reds += 1
            else:
                greens += 1
    return reds, greens


def solution(X, Y, colors):
    max_balanced_count = 0
    # The brute force method is to run through every point and create a circle
    # of that radius
    for i in range(len(X)):
        radius = dist(0, 0, X[i], Y[i])
        reds, greens = red_green_count_in_circle(Circle(x=0, y=0, radius=radius), X, Y, colours=colors)
        if reds == greens:
            max_balanced_count = max(max_balanced_count, reds + greens)
    return max_balanced_count
