
from math import sqrt


def dist_to_centre(x, y):
    return sqrt(x * x + y * y)


def red_green_count(colour):
    red = 0
    green = 0
    if colour == 'R':
        red += 1
    else:
        green += 1
    return red, green


def calc_distances(X, Y):
    distances = []
    for i in range(len(X)):
        distances.append((i, dist_to_centre(X[i], Y[i])))
    return distances


def get_index(distance_tuple):
    return distance_tuple[0]


def get_distance(distance_tuple):
    return distance_tuple[1]


def solution(X, Y, colors):
    max_balanced_count = 0
    distances = calc_distances(X, Y)
    distances.sort(key=lambda x: x[1])
    # now that the list is sorted from smallest to largest we know that the red-green
    # count can be carried over from the previous iteration and we just need to add
    # the next value to either the red or green count and updated the max_balanced_count
    # The only hitch is that we have to add all the values that have an equal distance
    last_dist = get_distance(distances[0])
    reds, greens = red_green_count(colors[get_index(distances[0])])
    idx = 1
    while idx < len(distances):
        dist_tuple = distances[idx]
        idx += 1
        # if the distance is different it means we've totalled all the
        # equidistant points and can update the balanced count
        if get_distance(dist_tuple) != last_dist:
            last_dist = get_distance(dist_tuple)
            if reds == greens:
                max_balanced_count = max(max_balanced_count, reds + greens)
        red, green = red_green_count(colors[get_index(dist_tuple)])
        reds += red
        greens += green
    if reds == greens:
        max_balanced_count = max(max_balanced_count, reds + greens)
    return max_balanced_count
