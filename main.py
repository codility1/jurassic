import random
import cProfile
import pstats

from brute_force import solution as brute_force
from jurassic import solution as jurassic


def run_hard(X, Y, colours, function_to_profile):
    for _ in range(10):
        function_to_profile(X, Y, colours)


def profile(X, Y, colours, function_to_profile):
    print('Num points = {}'.format(len(X)))
    pr = cProfile.Profile()
    pr.enable()
    run_hard(X, Y, colours, function_to_profile)
    pr.disable()
    pstats.Stats(pr).sort_stats('time', 'cumulative').print_stats()


def main():
    random.seed(129)  # so random
    functions_to_profile = [brute_force, jurassic]
    for function_to_profile in functions_to_profile:
        print('Function: {}'.format(function_to_profile))
        print('Testing large set of random points:')
        N = 1000  # range 1 to 100k
        X = random.sample(range(-20000, 20000), N)
        Y = random.sample(range(-20000, 20000), N)
        colours = [random.randrange(0, 1) for _ in range(N)]
        colours = ['R' if x == 0 else 'G' for x in colours]
        profile(X, Y, colours, function_to_profile)


if __name__ == '__main__':
    main()
